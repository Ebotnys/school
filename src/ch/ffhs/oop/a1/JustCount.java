package ch.ffhs.oop.a1;

public class JustCount implements Runnable {
	private final Memory memory;
	private final Memory memory2;

	public JustCount(Memory memory, Memory memory2) {
		this.memory = memory;
		this.memory2 = memory2;
	}

	public void run() {
		String threadName = Thread.currentThread().getName();
		int i = 0;
		while (true) {
			i++;
			memory2.setI(i);
			memory.setI(i);
			System.out.println(threadName + "set i to: " + i);
		}

	}
}
