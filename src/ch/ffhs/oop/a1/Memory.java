package ch.ffhs.oop.a1;

public class Memory {
	private int i;

	public synchronized int getI() {
		this.notify();
		try {
			this.wait();
			return i;
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized void setI(int i) {
		this.i = i;
		this.notify();
		try {
			this.wait();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

}
