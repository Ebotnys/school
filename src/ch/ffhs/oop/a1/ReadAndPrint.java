package ch.ffhs.oop.a1;

public class ReadAndPrint implements Runnable {
	private final Memory memory;
	private final Memory memory2;

	public ReadAndPrint(Memory memory, Memory memory2) {
		this.memory = memory;
		this.memory2 = memory2;
	}

	@Override
	public void run() {
		String threadName = Thread.currentThread().getName();
		System.out.println("i'll print from queue");
		while (true) {
			System.out.println(threadName + " memory i is:" + memory.getI());
			System.out.println(threadName + " memory i is:" + memory2.getI());
		}
	}

}
