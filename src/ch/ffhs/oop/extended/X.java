package ch.ffhs.oop.extended;

public class X extends Thread {

	/**
	 * @param args
	 */
	public Z z;
	
	public X(Z zz){
		z = zz;
		z.n = 0;
	}

	public void run(){
		synchronized(z){
			z.n = 1;
			z.notify();
			System.out.print("K");
		}
	}
}
