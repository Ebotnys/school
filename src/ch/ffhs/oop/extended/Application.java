package ch.ffhs.oop.extended;

public class Application extends Thread {

	/**
	 * @param args
	 */
	public static X x;
	public static Y y;
	public static Z z;
	
	public void run(){
		z= new Z(); x= new X(z);
		y = new Y(z);
		
		System.out.print("C");
		execute1();
		z.h();
		execute2();	
	}
	
	public void execute1(){
		System.out.print("A");
		x.start();
	}
	
	public void execute2(){
		y.start();
		System.out.print("L");
	}

}
